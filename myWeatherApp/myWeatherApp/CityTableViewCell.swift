import UIKit
import Kingfisher

class CityTableViewCell: UITableViewCell {
    
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var iconImageView: UIImageView!
    
    func configure(with object: WeatherClass) {
        guard let icon = object.current?.condition?.icon else {return}
        let url = URL(string: "https:" + icon)
        self.timeLabel.text = object.location?.localtime
        self.cityLabel.text = object.location?.name
        self.descriptionLabel.text = object.current?.condition?.text
        self.tempLabel.text = String (object.current?.temp_c ?? 0.0) + " ºC"
        self.iconImageView.kf.setImage(with: url)
    }
    
    
    
    
}

