import Foundation

class CitiesViewModel {
    
    let apiAccessKey = "fa8b6d2669b240ba9fe141304200312"
    let apiURL = "https://api.weatherapi.com/v1/forecast.json?key="
    var cityNamesArray: [String] = []
    var citiesWeatherArray: Bindable<[WeatherClass]> = Bindable([])
    var data: WeatherClass?
    
    
    func loadCitiesArray() {
        self.cityNamesArray = []
        if let citiesArray = Manager.shared.loadCitiesArray() {
            for city in citiesArray {
                if let city = city.location?.name {
                    self.cityNamesArray.append(city)
                }
            }
        }
    }
    
    
    
    func sendRequestforCitiesArray() {
        
        for city in self.cityNamesArray {
            
            guard let url = URL(string: self.apiURL + self.apiAccessKey + "&q=" + city + "&days=3") else {return}
            
            var request = URLRequest(url: url)
            request.httpMethod = "GET"
            
            let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                if error == nil, let data = data {
                    do {
                        self.data = try JSONDecoder().decode(WeatherClass.self, from: data)
                        guard let data = self.data else { return }
                        self.citiesWeatherArray.value.append(data)
                    } catch {
                        print(error)
                    }
                } else {
                    print(error?.localizedDescription ?? "some error")
                }
            }
            task.resume()
        }
    }
  
    
}




