import Foundation

class Manager {
    
    static let shared = Manager()
    private init () {}
    
    var is_exist = false
    
    
    func saveCitiesArray(object: WeatherClass) {
        var citiesArray = Manager.shared.loadCitiesArray()
        if let arrayOfCities = citiesArray {
            for element in arrayOfCities {
                if element.location?.name == object.location?.name {
                    self.is_exist = true
                }
            }
        }
        if self.is_exist == false {
            citiesArray?.append(object)
            UserDefaults.standard.set(encodable: citiesArray, forKey: "weatherArray")
        }
        self.is_exist = false
    }
    
    
    func loadCitiesArray() -> [WeatherClass]? {
        let weatherArray = UserDefaults.standard.value([WeatherClass].self, forKey: "weatherArray")
        if let weatherArray = weatherArray {
            return weatherArray
        } else {
            return []
        }
    }
    
    
    
}

