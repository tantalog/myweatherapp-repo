import Foundation
import CoreLocation
import UIKit
import Kingfisher

class ViewModel: NSObject {
    
    let apiAccessKey = "fa8b6d2669b240ba9fe141304200312"
    let apiURL = "https://api.weatherapi.com/v1/forecast.json?key="
    var location = "minsk"
    var data: WeatherClass?
    private var locationManager = CLLocationManager()
    private var coordinate: CLLocationCoordinate2D?
    
    
    var iconLink: String = ""
    var firstIconLinc: String = ""
    var secondIconLinc: String = ""
    var thirdIconLinc: String = ""
    var time: Bindable<String> = Bindable("")
    var temperature: Bindable<String> = Bindable("")
    var windSpeed: Bindable<String> = Bindable("")
    var humidity: Bindable<String> = Bindable("")
    var type: Bindable<String> = Bindable("")
    var pressure: Bindable<String> = Bindable("")
    var city: Bindable<String> = Bindable("")
    var iconImage: Bindable<UIImage> = Bindable(UIImage())
    var firstDay: Bindable<String> = Bindable("")
    var secondDay: Bindable<String> = Bindable("")
    var thirdDay: Bindable<String> = Bindable("")
    var firstIcon: Bindable<UIImage> = Bindable(UIImage())
    var secondIcon: Bindable<UIImage> = Bindable(UIImage())
    var thirdIcon: Bindable<UIImage> = Bindable(UIImage())
    var firstMaxTemp: Bindable<String> = Bindable("")
    var secondMaxTemp: Bindable<String> = Bindable("")
    var thirdMaxTemp: Bindable<String> = Bindable("")
    var firstMinTemp: Bindable<String> = Bindable("")
    var secondMinTemp: Bindable<String> = Bindable("")
    var thirdMinTemp: Bindable<String> = Bindable("")
    var firstRainChance: Bindable<String> = Bindable("")
    var secondRainChance: Bindable<String> = Bindable("")
    var thirdRainChance: Bindable<String> = Bindable("")
    var firstWindSpeed: Bindable<String> = Bindable("")
    var secondWindSpeed: Bindable<String> = Bindable("")
    var thirdWindSpeed: Bindable<String> = Bindable("")
    
    
    
    
    func configLocation() {
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
        self.location = UserDefaults.standard.string(forKey: "city") ?? "minsk"
    }

    
    func refreshData() {
        
        self.location = self.city.value.replacingOccurrences(of: " ", with: "%20")
        if self.location.count < 2 {
            return
        }
        self.sendRequest()
    }
    
    
    func currentLocationAction() {
        guard let latitude = coordinate?.latitude,
              let longitude = coordinate?.longitude else {return}
        self.location = String(latitude) + "," + String(longitude)
        self.sendRequest()
    }
    
    
    func loadData() {
        guard let data = self.data else { return }
        Manager.shared.saveCitiesArray(object: data)
        guard let current = data.current else { return }
        guard let location = data.location else { return }
        guard let forecast = data.forecast else { return }
        
        UserDefaults.standard.set(self.location, forKey: "city")
        DispatchQueue.main.async {
            self.time.value = current.last_updated!
            self.temperature.value = String(current.temp_c ?? 0) + " ºC"
            self.windSpeed.value = String(current.wind_kph ?? 0) + " km/h"
            self.humidity.value = String(current.humidity ?? 0) + " %"
            self.pressure.value = String(current.pressure_mb ?? 0) + " MB"
            self.type.value = current.condition?.text ?? ""
            self.iconLink = current.condition?.icon ?? ""
            self.city.value = location.name ?? ""
            self.firstDay.value = forecast.forecastday?[0].date ?? ""
            self.secondDay.value = forecast.forecastday?[1].date ?? ""
            self.thirdDay.value = forecast.forecastday?[2].date ?? ""
            self.firstIconLinc = forecast.forecastday?[0].day?.condition?.icon ?? ""
            self.secondIconLinc = forecast.forecastday?[1].day?.condition?.icon ?? ""
            self.thirdIconLinc = forecast.forecastday?[2].day?.condition?.icon ?? ""
            self.firstMaxTemp.value = "max " + String(forecast.forecastday?[0].day?.maxtemp_c ?? 0.0) + "ºC"
            self.secondMaxTemp.value = "max " + String(forecast.forecastday?[1].day?.maxtemp_c ?? 0.0) + "ºC"
            self.thirdMaxTemp.value = "max " + String(forecast.forecastday?[2].day?.maxtemp_c ?? 0.0) + "ºC"
            self.firstMinTemp.value = "min " + String(forecast.forecastday?[0].day?.mintemp_c ?? 0.0) + "ºC"
            self.secondMinTemp.value = "min " + String(forecast.forecastday?[1].day?.mintemp_c ?? 0.0) + "ºC"
            self.thirdMinTemp.value = "min " + String(forecast.forecastday?[2].day?.mintemp_c ?? 0.0) + "ºC"
            self.firstWindSpeed.value = String(forecast.forecastday?[0].day?.maxwind_kph ?? 0.0) + " km/h"
            self.secondWindSpeed.value = String(forecast.forecastday?[1].day?.maxwind_kph ?? 0.0) + " km/h"
            self.thirdWindSpeed.value = String(forecast.forecastday?[2].day?.maxwind_kph ?? 0.0) + " km/h"
            self.firstRainChance.value = forecast.forecastday?[0].day?.daily_chance_of_rain ?? "" + " %"
            self.secondRainChance.value = forecast.forecastday?[1].day?.daily_chance_of_rain ?? "" + " %"
            self.thirdRainChance.value = forecast.forecastday?[2].day?.daily_chance_of_rain ?? "" + " %"

            guard let url = URL(string: "https:" + self.iconLink) else { return }
            guard let firstUrl = URL(string: "https:" + self.firstIconLinc) else { return }
            guard let secondUrl = URL(string: "https:" + self.secondIconLinc) else { return }
            guard let thirdUrl = URL(string: "https:" + self.thirdIconLinc) else { return }
            
            KingfisherManager.shared.retrieveImage(with: url) { result in
                let image = try? result.get().image
                if let image = image {
                    self.iconImage.value = image
                }
            }
            KingfisherManager.shared.retrieveImage(with: firstUrl) { result in
                let image = try? result.get().image
                if let image = image {
                    self.firstIcon.value = image
                }
            }
            KingfisherManager.shared.retrieveImage(with: secondUrl) { result in
                let image = try? result.get().image
                if let image = image {
                    self.secondIcon.value = image
                }
            }
            KingfisherManager.shared.retrieveImage(with: thirdUrl) { result in
                let image = try? result.get().image
                if let image = image {
                    self.thirdIcon.value = image
                }
            }
        }
    }
    
    func sendRequest() {
        guard let url = URL(string: self.apiURL + self.apiAccessKey + "&q=" + self.location + "&days=3") else {return}
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            if error == nil, let data = data {
                do {
                    self.data = try JSONDecoder().decode(WeatherClass.self, from: data)

                    self.loadData()
                } catch {
                    print(error)
                }
            } else {
                print(error?.localizedDescription ?? "some error")
            }
        }
        task.resume()
    }

    
}


extension ViewModel: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]){
        guard let location = manager.location else {return}
        self.coordinate = location.coordinate
    }
}
