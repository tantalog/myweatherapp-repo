import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var pressureLabel: UILabel!
    @IBOutlet weak var windSpeedLabel: UILabel!
    @IBOutlet weak var humidityLabel: UILabel!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var weatherIconImageView: UIImageView!
    
    @IBOutlet weak var firstDayLabel: UILabel!
    @IBOutlet weak var firstIcon: UIImageView!
    @IBOutlet weak var firstMaxTempLabel: UILabel!
    @IBOutlet weak var firstMinTempLabel: UILabel!
    @IBOutlet weak var firstRainChanceLabel: UILabel!
    @IBOutlet weak var firstWindSpeedLabel: UILabel!
    @IBOutlet weak var secondDayLabel: UILabel!
    @IBOutlet weak var secondIcon: UIImageView!
    @IBOutlet weak var secondMaxTempLabel: UILabel!
    @IBOutlet weak var secondMinTempLabel: UILabel!
    @IBOutlet weak var secondRainChanceLabel: UILabel!
    @IBOutlet weak var secondWindSpeedLabel: UILabel!
    @IBOutlet weak var thirdDayLabel: UILabel!
    @IBOutlet weak var thirdIcon: UIImageView!
    @IBOutlet weak var thirdMaxTempLabel: UILabel!
    @IBOutlet weak var thirdMinTempLabel: UILabel!
    @IBOutlet weak var thirdRainChanceLabel: UILabel!
    @IBOutlet weak var thirdWindSpeedLabel: UILabel!
    
    
    private var viewModel = ViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewModel.configLocation()
        self.viewModel.sendRequest()
        self.configureUI()
        createSwipeRecognizer()
        self.hideKeyboard()
        self.searchTextField.delegate = self    
    }
    

    
    func createSwipeRecognizer() {
        let rightSwipeRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(moveToCitiesViewController(swipe:)))
        rightSwipeRecognizer.direction = .right
        self.view.addGestureRecognizer(rightSwipeRecognizer)
    }
    
    @IBAction func refreshButtonPressed(_ sender: UIButton) {
        guard let city = self.searchTextField?.text else {return}
        self.viewModel.city.value = city
        self.viewModel.refreshData()
        self.configureUI()
    }
    @IBAction func locationButtonPressed(_ sender: UIButton) {
        self.viewModel.currentLocationAction()
        self.configureUI()
    }
    
    private func configureUI() {
        
        self.viewModel.temperature.bind { (text) in
            self.tempLabel?.text = text
        }
        self.viewModel.windSpeed.bind { (text) in
            self.windSpeedLabel?.text = text
        }
        self.viewModel.humidity.bind { (text) in
            self.humidityLabel?.text = text
        }
        self.viewModel.pressure.bind { (text) in
            self.pressureLabel?.text = text
        }
        self.viewModel.type.bind { (text) in
            self.descriptionLabel?.text = text
        }
        self.viewModel.city.bind { (text) in
            self.locationLabel?.text = text
        }
        self.viewModel.iconImage.bind { (image) in
            self.weatherIconImageView?.image = image
        }
        self.viewModel.firstDay.bind { (text) in
            self.firstDayLabel?.text = text
        }
        self.viewModel.secondDay.bind { (text) in
            self.secondDayLabel?.text = text
        }
        self.viewModel.thirdDay.bind { (text) in
            self.thirdDayLabel?.text = text
        }
        self.viewModel.firstIcon.bind { (image) in
            self.firstIcon?.image = image
        }
        self.viewModel.secondIcon.bind { (image) in
            self.secondIcon?.image = image
        }
        self.viewModel.thirdIcon.bind { (image) in
            self.thirdIcon?.image = image
        }
        self.viewModel.firstMaxTemp.bind { (text) in
            self.firstMaxTempLabel?.text = text
        }
        self.viewModel.secondMaxTemp.bind { (text) in
            self.secondMaxTempLabel?.text = text
        }
        self.viewModel.thirdMaxTemp.bind { (text) in
            self.thirdMaxTempLabel?.text = text
        }
        self.viewModel.firstMinTemp.bind { (text) in
            self.firstMinTempLabel?.text = text
        }
        self.viewModel.secondMinTemp.bind { (text) in
            self.secondMinTempLabel?.text = text
        }
        self.viewModel.thirdMinTemp.bind { (text) in
            self.thirdMinTempLabel?.text = text
        }
        self.viewModel.firstRainChance.bind { (text) in
            self.firstRainChanceLabel?.text = text
        }
        self.viewModel.secondRainChance.bind { (text) in
            self.secondRainChanceLabel?.text = text
        }
        self.viewModel.thirdRainChance.bind { (text) in
            self.thirdRainChanceLabel?.text = text
        }
        self.viewModel.firstWindSpeed.bind { (text) in
            self.firstWindSpeedLabel?.text = text
        }
        self.viewModel.secondWindSpeed.bind { (text) in
            self.secondWindSpeedLabel?.text = text
        }
        self.viewModel.thirdWindSpeed.bind { (text) in
            self.thirdWindSpeedLabel?.text = text
        }
    }
    
    @IBAction func moveToCitiesViewController(swipe: UISwipeGestureRecognizer) {

        guard let citiesViewContoller = self.storyboard?.instantiateViewController(withIdentifier: "CitiesViewController") as? CitiesViewController else {
            return
        }
        self.navigationController?.pushViewController(citiesViewContoller, animated: true)
    }
    
}

