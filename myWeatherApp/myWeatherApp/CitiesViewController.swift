import UIKit

class CitiesViewController: UIViewController {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var citiesTableView: UITableView!
    
    private var citiesViewModel = CitiesViewModel()
    var citiesWeatherArray: [WeatherClass] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.citiesViewModel.loadCitiesArray()
        self.citiesViewModel.sendRequestforCitiesArray()
        self.bindCitiesWeatherArray()
        self.createSwipeRecognizer()
    }

    
    private func bindCitiesWeatherArray() {
        self.citiesViewModel.citiesWeatherArray.bind { (citiesArray) in
            self.citiesWeatherArray = citiesArray
            DispatchQueue.main.async {
                self.citiesTableView.reloadData()
            }
        }
    }
    
    func createSwipeRecognizer() {
        let leftSwipeRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(moveToCitiesViewController(swipe:)))
        leftSwipeRecognizer.direction = .left
        self.view.addGestureRecognizer(leftSwipeRecognizer)
    }
    
    @IBAction func moveToCitiesViewController(swipe: UISwipeGestureRecognizer) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    
}

extension CitiesViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.citiesWeatherArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CityTableViewCell", for: indexPath) as? CityTableViewCell else {
            return UITableViewCell()
        }
        cell.configure(with: citiesWeatherArray[indexPath.row])
        return cell
    }
    
}
